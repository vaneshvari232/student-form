<?php
class ActivityModel extends CI_Model {

    private $subject_table = 'master_activity';

    function __construct() {
        parent::__construct();
    }

    function insert($sid) {
         $extra=$sub=$this->input->post('extra');
       $data1 = array('activity'=>$extra,'sid'=>$sid);
        $result = $this->db->insert($this->subject_table, $data1);
        if ($result !== NULL) {
            return TRUE;
        }
        return FALSE;
    }

}