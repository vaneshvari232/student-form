<?php



class SubjectModel extends CI_Model {

    private $skill_table = 'master_subjects';
    
    function __construct() {
        parent::__construct();
    }

    function insert($sid) {
         $sub=$this->input->post('sub');
          $sub1=$this->input->post('sub1');
          $sub2=$this->input->post('sub2');
          $sub3=$this->input->post('sub3');
          $sub4=$this->input->post('sub4');
          $sub5=$this->input->post('sub5');
        $data1 = array('subjectname'=>$sub,'s1'=>$sub1,'s2'=>$sub2,'s3'=>$sub3,'s4'=>$sub4,'s5'=>$sub5,'sid'=>$sid);
        $result = $this->db->insert($this->skill_table, $data1);
        if ($result !== NULL) {
            return TRUE;
        }
        return FALSE;
    }

}