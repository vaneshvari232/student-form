<?php

class UserModel extends CI_Model {

    private $user_table = 'student';

    function __construct() {
        parent::__construct();
    }

    function insert_user($fname,$lname,$email,$password,$confirmpassword,$state,$city,$phone,$pemail,$insta,$webs) {
        $data = array('firstname' => $fname, 'lastname' => $lname, 'emailid' => $email, 'password' => $password, 'confirmpassword' => $confirmpassword, 'state' => $state, 'city' => $city,'phone'=>$phone,'parentsemail'=>$pemail,'instagramhandle'=>$insta,'website'=>$webs);
        $sid= $this->db->insert($this->user_table, $data);
         $this->highmodel->insert_high($sid);
         $this->subjectmodel->insert($sid);
         $this->skillmodel->insert_skill($sid);
         $this->activitymodel->insert($sid);
         $this->careermodel->insert($sid);
        if ($sid!== NULL) {
            return TRUE;
        }
        return FALSE;
    }

}