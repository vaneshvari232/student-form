
<?php

class HighModel extends CI_Model {

    private $high_table= 'studenthighschool';
   

    function __construct() {
        parent::__construct();
            $this->load->database();
    }

    function insert_high($sid) {
          $state=$this->input->post('state');
          $city=$this->input->post('city');
          $schoolname=$this->input->post('schoolname');
          $gp=$this->input->post('gp');
          $gpa=$this->input->post('gpa');
   $data1 = array('state'=>$state,'city' => $city,'schoolname' => $schoolname, 'gpa_unweighted' => $gp, 'gpa_weighted' => $gpa,'sid'=>$sid);
         $this->db->trans_start();
         
        $this->db->insert($this->high_table, $data1);
        $this->db->trans_complete();
        if ($this->db->trans_status()===FALSE) {
            return -1;
        }
        else
        {
            return $this->db->insert_id();
        }
    }

}