
<?php

class ProjectModel extends CI_Model {

    private $pro_table= 'projects';
   

    function __construct() {
        parent::__construct();

    }

    function proinsert( array $data)    {

        $this->db->trans_start();
         
        $this->db->insert($this->pro_table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status()===FALSE) {
            return -1;
        }
        else
        {
            return $this->db->insert_id();
        }
    } 

}
