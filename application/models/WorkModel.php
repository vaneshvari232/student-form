 <?php
class WorkModel extends CI_Model {

    private $work_table='workexperience';
    function __construct() {
        parent::__construct();
    }

    function insert(array $data) {
       
        $this->db->trans_start();
         
        $this->db->insert($this->work_table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status()===FALSE) {
            return -1;
        }
        else
        {
            return $this->db->insert_id();
        }
    } 

}