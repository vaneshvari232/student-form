<html>
    <head>
        <title>studentform</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/form.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
        <!-- searchable content-->
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/bundle.min.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/Element.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/multi-select2.js"></script>
        <!--end of searchable content-->
         <!--end of add div content-->
   <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addbootstrap.min.js"></script>

          <!--end of add div content-->
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/form.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/validate.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addrow.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addproject.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addaward.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addcerti.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/addwork.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/sweetalert.min.js"></script>
       
       
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/bootstrap.min.js"></script> 
    
<!--//////////-->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/tagcomplete.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/tagcomplete.material.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/tagcomplete.material.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/tagcomplete.min.css">
      <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/tagcomplete.js"></script>
        <script type = 'text/javascript' src = "<?php echo base_url();  ?>js/tagcomplete.min.js"></script>
     <script>
$(function(){
    var data = [
        'Active Listening',
        'Adaptability',
        'Communication',
        'Creativity',
        'Critical Thinking',
        'Customer Service',
        'Decision Making',
        'Interpersonal Communication',
        'Management',
        'Leadership',
        'Organization',
        'Public Speaking',
        'Problem Solving',
        'Teamwork'
    ];

    $(".tags_input").tagComplete({

                keylimit: 1,
                hide: false,
                autocomplete: {
                    data: data
                }
        });
});
</script> 
<!-------//////////////////-->

    </head>
    <body>
                        <?php echo $this->session->userdata('session_id()'); ?> 
        <ul id="signup-step">
            <li id="personal" class="active">Personal Information</li>
            <li id="high">High School</li>
            <li id="skills">Skills And Activities</li>
             <li id="awards">Awards And Certification</li>
              <li id="work">Work Experience</li>
        </ul>
              
        <?php
        if (isset($success)) {
             ?>      <script >
               swal({ title: "Good job!", text: "User record inserted successfully",icon: "success",button: "Aww yess!",});
                     </script>
     <?php       
        }

        $attributes = array('name' => 'frmRegistration', 'id' => 'signup-form');
        echo form_open_multipart($this->uri->uri_string(), $attributes);
        ?> 
        <div id="personal-field" class="mt-4">
              <div class="row">
                

             <div class="col-md-6"><label>First Name</label><span id="fname-error" class="signup-error"></span>
            <div><input type="text" name="fname" id="fname" class="form-control"/></div></div>
           <div class="col-md-6"> <label>Last Name</label><span id="lname-error" class="signup-error"></span>
            <div><input type="text" name="lname" id="lname" class="form-control"/></div></div>
           <div class="col-md-6"> <label>Email</label><span id="email-error" class="signup-error"></span>
            <div><input type="text" name="email" id="email" class="form-control" /></div></div>
            <div class="col-md-6"> <label>Enter Password</label><span id="password-error" class="signup-error"></span>
            <div><input type="password" name="password" id="user-password" class="form-control" /></div></div>
           <div class="col-md-6"> <label>Confirm Password</label><span id="confirm-password-error" class="signup-error"></span>
            <div><input type="password" name="confirmpassword" id="confirmpassword" class="form-control" /></div></div>
            
           <div class="col-md-6"> <label>State</label>
            <div>
                <select name="state" id="state" class="form-control" required>
                   <option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Lakshadweep">Lakshadweep</option><option value="Puducherry">Puducherry</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option>                                                                         
                </select>
            </div></div>
          <div class="col-md-6">  <label>City</label><span id="city-error" class="signup-error"></span>
            <div>
                <select name="city" id="city" class="form-control" required>
                      <option value="Raipur">Raipur</option>
                      <option value="Durg">Durg</option>
                      <option value="Bilaspur">Bilaspur</option>
                      <option value="bhopal">Bhopal</option>
                     <option value="Nagpur">Nagpur</option></select>
            </div></div>
           <div class="col-md-6"> <label>Phone Number</label><span id="phone-error" class="signup-error"></span>
            <div><input type="text" name="phone" id="phone" class="form-control" /></div></div>
            <div class="col-md-6"><label>Parent's Email</label><span id="pemail-error" class="signup-error"></span>
            <div><input type="text" name="pemail" id="pemail" class="form-control" /></div></div>
           <div class="col-md-6"> <label>Instagram Handle</label><span id="insta-error" class="signup-error"></span>
            <div><input type="text" name="insta" id="insta" class="form-control" placeholder="@" /></div></div>
            <div class="col-md-6"> <label>Website</label><span id="web-error" class="signup-error"></span>
            <div><input type="text" name="webs" id="web" class="form-control" placeholder="" /></div></div>
        </div></div>

        <div id="high-field" style="display:none;" class="mt-4">
           <div class="row">
            
            <div class="col-md-6"> <label>City</label>
            <div>
                <select name="city" id="city" class="form-control" required>
                      <option value="Raipur">Raipur</option>
                      <option value="Durg">Durg</option>
                      <option value="Bilaspur">Bilaspur</option>
                      <option value="bhopal">Bhopal</option>
                     <option value="Nagpur">Nagpur</option></select>
            </div></div>
              <div class="col-md-6"><label> School Name</label> 
             <div>
                <select name="schoolname" id="schoolname" class="form-control">
                     <option value="kv">Kendriya Vidyalaya</option>
                     <option value="Dps">Delhi Public School</option>
                     <option value="kps">Khalsa Public School</option>
                    <option value="krps">Krishna Public School</option>
                    <option value="Nps">Natraj Public School</option></select>
             </div></div>
             <div class="col-md-6"> <label> High School Subjects</label>&nbsp<input type="button" value="+" onclick="addRow()">
             <span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub" id="sub" class="form-control" placeholder="subject" /></div>
            <div id="content"></div>
             <div class="col-md-6"> <label style="color: white;">ghghggh </label><span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub1" id="sub" class="form-control" placeholder="subject" /></div>
             <div class="col-md-6"> <label style="color: white;">kkk </label><span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub2" id="sub" class="form-control" placeholder="subject" /></div>
             <div class="col-md-6"> <label style="color: white;">kkk</label><span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub3" id="sub" class="form-control" placeholder="subject" /></div>
             <div class="col-md-6"> <label style="color: white;">llll</label><span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub4" id="sub" class="form-control" placeholder="subject" /></div>
             <div class="col-md-6"> <label style="color: white;">kkkk</label><span id="sub-error" class="signup-error"></span>
             <input type="text" name="sub5" id="sub" class="form-control" placeholder="subject" /></div>
             
            <div class="col-md-6"> <label> GPA(unweighted)</label><span id="gp-error" class="signup-error"></span> <div>
              <select name="gp" id="gp" class="form-control" required>
                      <option value="4.0">4.0</option>
                      <option value="3.7">3.7</option>
                      <option value="3.3">3.3</option>
                      <option value="3.0">3.0</option>
                     <option value="2.7">2.7</option>
                       <option value="2.3">2.3</option>
                      <option value="2.0">2.0</option>
                      <option value="1.7">1.7</option>
                      <option value="1.3">1.3</option>
                     <option value="1.0">1.0</option>
                     <option value="0.0">0.0</option>
                   </select></div></div>
          
             <div class="col-md-6"><label> GPA(weighted)</label><span id="gp-error" class="signup-error"></span> <div>
              <select name="gpa" id="gpa" class="form-control" required>
                      <option value="5.0">5.0</option>
                      <option value="4.7">4.7</option>
                      <option value="4.3">4.3</option>
                      
                      <option value="4.0">4.0</option>
                      <option value="3.7">3.7</option>
                      <option value="3.3">3.3</option>
                      <option value="3.0">3.0</option>
                     <option value="2.7">2.7</option>
                       <option value="2.3">2.3</option>
                      <option value="2.0">2.0</option>
                      <option value="1.7">1.7</option>
                      <option value="1.3">1.3</option>
                     <option value="1.0">1.0</option>
                     <option value="0.0">0.0</option>
                   </select></div>
        </div></div> 
     
   

      </div>
        
        <div id="skills-field" style="display:none;">
          <label>Skills</label><span id="skill-error" class="signup-error"></span>
         <div><textarea id="skill" name="skill" rows="10" cols="50"  class='tags_input'  >
        </textarea></div>
          
       
              <label> extracurricular activities</label><span id="extra-error" class="signup-error"></span>
             <div> <textarea id="extra" name="extra" rows="4" cols="50" class="demoInputBox"></textarea></div>
         </div>
                
        
        <div id="awards-field" style="display:none;" >
                 <div id="project_field">
          <label> Projects</label>&nbsp<button type="button" name="add" id="add" class="btn btn-success">+</button>
            <span id="pro-error" class="signup-error"></span>
            
              
                <table class="table table-bordered" id="dynamic_field">  <div>
                 Project Title<div><input type="text" name="pro[]" placeholder="project title" class="demoInputBox"></div>
           
              Description<div><input type="text" name="des[]" placeholder="description" class="demoInputBox"></div>
                Start Date<div><input type="date" name="sd[]" class="demoInputBox"></div>
                End Date<div><input type="date" name="ed[]" class="demoInputBox"></div>
               </div>
          </table></div>
           <div id="award_field">  <label> Awards</label>&nbsp<button type="button" name="addition" id="addition" class="btn btn-success">+</button>
            <span id="award" class="signup-error"></span>
                 
                  <table class="table table-bordered" id="dynamic_field">   <input type="text" id="award" name="award[]" placeholder="award" class="form-control"></table>
              </div>
     <div id="certi_field">    <label>Certifications</label>&nbsp<button type="button" name="certi" id="certi" class="btn btn-success">+</button>

        
                   
      <table class="table table-bordered" id="dynamic_field"> <input type="text" id="certi" name="certi[]" placeholder="certificate" class="form-control"></div></table>
 </div></div>
                   
        <div id="work-field" style="display:none;" >
           <div id="work_field">
             
          <label>Work Experience</label>&nbsp<button type="button" name="centis" id="centis" class="btn btn-success">+</button>
          <span  id="work-error" class="signup-error"></span>
                <table class="table table-bordered" id="dynamic_field">
              <div> <input type="text" name="work[]" placeholder="title" id="work" class="demoInputBox"></div>
            <div> <input type="text" name="companyname[]" placeholder="company name" class="demoInputBox"></div>
              <div><input type="text" name="location[]" placeholder="location" class="demoInputBox"></div>
          <div><input type="date" name="startdate[]" placeholder="start date" class="demoInputBox"></div>
              <div><input type="date" name="enddate[]" placeholder="end date" class="demoInputBox"></div>
            </table></div>
       <label>Career Goals</label><span id="goal-error" class="sign-up signup-error"> </span>
              <div>
                 <textarea id="goal" name="goal" rows="4" cols="50"></textarea>
               </div>
      <label>Resume(Optional)</label>
      <div> <input type="file" name="ufile"  id="Inputfile" class="form-control"><br></strong></i></div></div>
      
        <div>
            <input class="btnAction" type="button" name="back" id="back" value="Previous" style="display:none;">
            <input class="btnAction" type="button" name="next" id="next" value="Next" >
            <input class="btnAction" type="submit" name="finish" id="finish" value="Finish" style="display:none;">
        </div>
        <?php echo form_close(); ?>
   <script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  
   
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td> Project Title<div><input type="text" name="pro[]" placeholder="project title" class="demoInputBox"><br><input type="text" name="des[]" placeholder="description" class="demoInputBox"><br><input type="date" name="sd[]" class="demoInputBox"><br><input type="date" name="ed[]" class="demoInputBox"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
</script>
<script type="text/javascript">
  $(document).ready(function(){      
      var i=1;  
   
      $('#addition').click(function(){  
           i++;  
           $('#award_field').append('<tr id="row'+i+'" class="dynamic-added"><td>    <input type="text" id="award" name="award[]" placeholder="award" class="demoInputBox"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
</script>
<script type="text/javascript">
  $(document).ready(function(){      
      var i=1;  
   
      $('#certi').click(function(){  
           i++;  
           $('#certi_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" id="certi" name="certi[]" placeholder="certificate" class="form-control">    </td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
</script>
<script type="text/javascript">
  $(document).ready(function(){      
      var i=1;  
   
      $('#centis').click(function(){  
           i++;  
           $('#work_field').append('<tr id="row'+i+'" class="dynamic-added"><td> <input type="text" name="work[]" placeholder="title" id="work" class="demoInputBox"></div><div> <input type="text" name="companyname[]" placeholder="company name" class="demoInputBox"></div><div><input type="text" name="location[]" placeholder="location" class="demoInputBox"></div> <div><input type="date" name="startdate[]" placeholder="start date" class="demoInputBox"></div> <div><input type="date" name="enddate[]" placeholder="end date" class="demoInputBox"></div>   </td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
</script>
</body>
</html>
    </body>
</html>