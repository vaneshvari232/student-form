<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper(array('form', 'url'));
        $this->load->model('upload_model');
        $this->load->model('usermodel');
        $this->load->model('highmodel');
        $this->load->model('subjectmodel');
        $this->load->model('skillmodel');
        $this->load->model('activitymodel');
         $this->load->model('projectmodel');
       $this->load->model('awardmodel');
         $this->load->model('workmodel');
       $this->load->model('careermodel');
    }

    public function index() {
   if ($this->input->post('finish')) {

         //personal details
            $this->form_validation->set_rules('fname', 'firstname', 'trim|required');
            $this->form_validation->set_rules('lname', 'lastname', 'trim|required');
            $this->form_validation->set_rules('email', 'Email Address', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            $this->form_validation->set_rules('confirmpassword', 'confirmpassword', 'trim|required');
            $this->form_validation->set_rules('state', 'state', 'trim|required');
            $this->form_validation->set_rules('city', 'city', 'trim|required');
             $this->form_validation->set_rules('phone', 'phone', 'trim|required');
             $this->form_validation->set_rules('pemail', 'pemail', 'trim|required');
             $this->form_validation->set_rules('insta', 'insta', 'trim|required');
            $this->form_validation->set_rules('webs', 'webs', 'trim|required');

                 //highschool
               $this->form_validation->set_rules('state', 'state', 'trim|required');
              $this->form_validation->set_rules('city', 'city', 'trim|required');
              $this->form_validation->set_rules('schoolname', 'schoolname', 'trim|required');
              $this->form_validation->set_rules('gp', 'gp', 'trim|required');
              $this->form_validation->set_rules('gpa', 'gpa', 'trim|required');

                   //subject
              $this->form_validation->set_rules('sub', 'subjects', 'trim|required');
              //skill
                $this->form_validation->set_rules('skill', 'skills', 'trim|required');
                //activity
                    $this->form_validation->set_rules('extra', 'activity', 'trim|required');

                     //project
                      $this->form_validation->set_rules('pro[]', 'project', 'trim|required');
                      $this->form_validation->set_rules('des[]', 'description', 'trim|required');
                      $this->form_validation->set_rules('sd[]', 'startdate', 'trim|required');
                      $this->form_validation->set_rules('ed[]', 'enddate', 'trim|required');
                   //awards
                   $this->form_validation->set_rules('award[]', 'awards', 'trim|required');
                   $this->form_validation->set_rules('certi[]', 'certificate', 'trim|required');
                   //workexperience
                   $this->form_validation->set_rules('work[]', 'works', 'trim|required');
                   $this->form_validation->set_rules('companyname[]', 'companyname', 'trim|required');
                   $this->form_validation->set_rules('location[]', 'location', 'trim|required');
                   $this->form_validation->set_rules('startdate[]', 'startdate', 'trim|required');
                   $this->form_validation->set_rules('enddate[]', 'enddate', 'trim|required');
                   //career
                    $this->form_validation->set_rules('goal', 'goal', 'trim|required');
                   
            if ($this->form_validation->run() !== FALSE) {
                $result1 = $this->usermodel->insert_user($this->input->post('fname'), $this->input->post('lname'), $this->input->post('email'), $this->input->post('password'), $this->input->post('confirmpassword'), $this->input->post('state'), $this->input->post('city'), $this->input->post('phone'), $this->input->post('pemail'), $this->input->post('insta'), $this->input->post('webs'));
                 $result2=$this->highmodel->insert_high($this->input->post('state'),$this->input->post('city'),$this->input->post('schoolname'),$this->input->post('gp'),$this->input->post('gpa'));
               //result3
                 $result3=$this->subjectmodel->insert($this->input->post('sub'),$this->input->post('sub1'),$this->input->post('sub2'),$this->input->post('sub3'),$this->input->post('sub4'),$this->input->post('sub5'));
                 //result4
                     $result4=$this->skillmodel->insert_skill($this->input->post('skill'));
                     //result5
                      $result5=$this->activitymodel->insert($this->input->post('extra'));
                      //result6
                     // $result6=$this->projectmodel->insert($this->input->post('pro[]'),$this->input->post('des[]'),$this->input->post('sd[]'),$this->input->post('ed[]'));
                        
               
      
        
              
                     // $result7=$this->awardmodel->insert($this->input->post('award'),$this->input->post('certi'));
                        //$result8=$this->workmodel->insert($this->input->post('work'),$this->input->post('companyname'),$this->input->post('location'),$this->input->post('startdate'),$this->input->post('enddate'));
                        //result9
                       $result9=$this->careermodel->insert($this->input->post('goal'));
                $result=array('res1'=>$result1,'res2'=>$result2,'res3'=>$result3,'res4'=>$result4,'res5'=>$result5,'res9'=>$result9);
                $data['success'] = $result;
                $this->load->view('user', $data);
                $this->do_upload();
                $this->project();
                $this->awards();
                $this->work();
              
            } else {
                $this->load->view('user');
            }
        } else {
            $this->load->view('user');
        }
    }

public function do_upload(){
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'gif|jpg|png|pdf';
  $config['max_size']             = 100000;

  $this->load->library('upload', $config);

  if ( ! $this->upload->do_upload('ufile'))
  {
      $error = array('error' => $this->upload->display_errors());

      $this->load->view('user', $error);
  }
  else
  {
    //upload files to server and insert into database
    $datav = $this->upload->data();
//insert query here using model

$data = array('upload_data' => $datav);

$this->load->view('user', $data);
    $this->upload_model->insert_upload($datav['file_name'],$datav['file_type'],$datav['file_size'],$datav['file_path']);
//insert query here using model


  }
}
public function project()
 {

   

if (isset($_POST['pro'])) {

$project=$_POST['pro'];
$project_count=count($project);
for ($i=0; $i<$project_count; $i++) {

$projectData=array (

'ptitle'=>$_POST['pro'][$i],
'description'=>$_POST['des'][$i],
'startdate'=>$_POST['sd'][$i],
'enddate'=>$_POST['ed'][$i],
);
$this->projectmodel->proinsert($projectData);
}

}
}
public function awards()
 {

   

if (isset($_POST['award'])) {

$award=$_POST['award'];
$award_count=count($award);
for ($i=0; $i<$award_count; $i++) {

$awardData=array (

'awardname'=>$_POST['award'][$i],
'certificates'=>$_POST['certi'][$i],
);
$this->awardmodel->insert($awardData);
}

}
}
public function work()
 {

   

if (isset($_POST['work'])) {

$work=$_POST['work'];
$work_count=count($work);
for ($i=0; $i<$work_count; $i++) {

$workData=array (

'title'=>$_POST['work'][$i],
'companyname'=>$_POST['companyname'][$i],
'location'=>$_POST['location'][$i],
'startdate'=>$_POST['startdate'][$i],
'enddate'=>$_POST['enddate'][$i],
);
$this->workmodel->insert($workData);
}

}
}


}
