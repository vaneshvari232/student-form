
            function validate() {
                var output = true;
                $(".signup-error").html('');

                if ($("#personal-field").css('display') != 'none') {
                    if (!($("#fname").val())) {
                        output = false;
                        $("#fname-error").html("Name required!");
                    }
                    if (!($("#lname").val())) {
                        output = false;
                        $("#lname-error").html("Name required!");
                    }
                    if (!($("#email").val())) {
                        output = false;
                        $("#email-error").html("email required!");
                    }
                   if (!($("#user-password").val())) {
                        output = false;
                        $("#password-error").html("Password required!");
                    }

                    if (!($("#confirmpassword").val())) {
                        output = false;
                        $("#confirm-password-error").html("Confirm password required!");
                    }

                    if ($("#user-password").val() != $("#confirmpassword").val()) {
                        output = false;
                        $("#confirm-password-error").html("Password not matched!");
                    }
                    
                 
                    if (!($("#phone").val())) {
                        output = false;
                        $("#phone-error").html("Phone required!");
                    }

                    if (!($("#pemail").val())) {
                        output = false;
                        $("#pemail-error").html("email required!");
                    }
                 
                }

                if ($("#high-field").css('display') != 'none') {
                    if (!($("#sub").val())) {
                        output = false;
                        $("#sub-error").html("subject required!");
                    }

                    if (!($("#gp").val())) {
                        output = false;
                        $("#gp-error").html("enter GPA!");
                    }
                    if (!($("#gpa").val())) {
                        output = false;
                        $("#gpa-error").html("enter GPA!");
                    }

                    
                }


                if ($("#skills-field").css('display') != 'none') {
                    if (!($("#skill").val())) {
                        output = false;
                        $("#skill-error").html("skills required!");
                    }

                    if (!($("#extra").val())) {
                        output = false;
                        $("#extra-error").html("enter activities!");
                    }
                   
                    
                }
                     
               
                 if ($("#work-field").css('display') != 'none') {
                    if (!($("#goal").val())) {
                        output = false;
                        $("#goal-error").html("Goal required!");
                    }

                    
                }
                return output;
            }

            $(document).ready(function () {
                $("#next").click(function () {
                    var output = validate();
                    if (output === true) {
                        var current = $(".active");
                        var next = $(".active").next("li");
                        if (next.length > 0) {
                            $("#" + current.attr("id") + "-field").hide();
                            $("#" + next.attr("id") + "-field").show();
                            $("#back").show();
                            $("#finish").hide();
                            $(".active").removeClass("active");
                            next.addClass("active");
                            if ($(".active").attr("id") == $("li").last().attr("id")) {
                                $("#next").hide();
                                $("#finish").show();
                            }
                        }
                    }
                });


                $("#back").click(function () {
                    var current = $(".active");
                    var prev = $(".active").prev("li");
                    if (prev.length > 0) {
                        $("#" + current.attr("id") + "-field").hide();
                        $("#" + prev.attr("id") + "-field").show();
                        $("#next").show();
                        $("#finish").hide();
                        $(".active").removeClass("active");
                        prev.addClass("active");
                        if ($(".active").attr("id") == $("li").first().attr("id")) {
                            $("#back").hide();
                        }
                    }
                });

                $("input#finish").click(function (e) {
                    var output = validate();
                    var current = $(".active");

                    if (output === true) {
                        return true;
                    } else {
                        //prevent refresh
                        e.preventDefault();
                        $("#" + current.attr("id") + "-field").show();
                        $("#back").show();
                        $("#finish").show();
                    }
                });
            });
      